
import java.util.Scanner;
public class Taschenrechner
{
 public static void main(String[] args)
 {
 Scanner einlesen = new Scanner (System.in);
 int zahl1 = 0, zahl2 = 0, ergebnis = 0;
 System.out.print("Bitte geben Sie eine ganze Zahl ein : ");
 zahl1 = einlesen.nextInt();
 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
 zahl2 = einlesen.nextInt();
 ergebnis = zahl1 + zahl2;
 System.out.println("Ergebnis der Addition lautet: ");
 System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
 }
}