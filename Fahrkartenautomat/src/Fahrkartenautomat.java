import java.util.Scanner;

class Fahrkartenautomat {

    public static double fahrkartenbestellungErfassen() {
    	String[] Bezeichnung = new String[11];
    	Bezeichnung[1] = "Einzelfahrschein Berlin AB";
    	Bezeichnung[2] = "Einzelfahrschein Berlin BC";
    	Bezeichnung[3] = "Einzelfahrschein Berlin ABC";
    	Bezeichnung[4] = "Kurzstrecke";
    	Bezeichnung[5] = "Tageskarte Berlin AB";
    	Bezeichnung[6] = "Tageskarte Berlin BC";
    	Bezeichnung[7] = "Tageskarte Berlin ABS";
    	Bezeichnung[8] = "Kleingruppen-Tageskarte Berlinn AB";
    	Bezeichnung[9] = "Kleingruppen-Tageskaarte Berlin BC";
    	Bezeichnung[10] = "Kleingruppen-Tageskarte Berlin ABC";
    	
    	double[] Preis = new double[11];
    	Preis[1] = 2.90;
    	Preis[2] = 3.30;
    	Preis[3] = 3.60;
    	Preis[4] = 1.90;
    	Preis[5] = 8.60;
    	Preis[6] = 9.00;
    	Preis[7] = 9.60;
    	Preis[8] = 23.50;
    	Preis[9] = 24.30;
    	Preis[10] = 24.90;
        int anzahlTickets;
        double ticketPreis;
        Scanner tastatur = new Scanner(System.in);

        System.out.print("Ticketpreis (EURO-Cent): ");
        ticketPreis = tastatur.nextDouble();

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();

        return ticketPreis * anzahlTickets;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f � %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-M�zen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {

        double zuZahlenderBetrag;
        double rueckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rueckgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}