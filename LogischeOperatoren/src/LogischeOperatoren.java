
public class LogischeOperatoren {

	public static void main(String[] args) {
		/* 1. Deklarieren Sie zwei Wahrheitswerte a und b.*/
		
		boolean a;
		boolean b;
		
		/* 2. Initialisieren Sie einen Wert mit true, den anderen mit falsea.a */
		a = false;
		b = true;
		
		/* 3.a Geben Sie beide Werte aus.*/
		System.out.println(a);
		System.out.println(b);
		
		/* 4. Deklarieren Sie einen Wahrheitswert undGatter*/
		
		boolean undGatter;
		
		/* 5. Weisen Sie der Variable undGatter den Wert "a AND b" zu und geben Sie das Ergebnis aus. */
		
		undGatter = a && b;
		
		System.out.println(undGatter);
		
		/* 6. Deklarieren Sie au�erdem den Wahrheitswert c und initialisieren ihn direkt mit dem Wert true. */
		
		boolean c = true;
		
		/* 7. Verkn�pfen Sie alle drei Wahrheitswerte a, b und c und geben Sie jeweils das Ergebnis aus. */
		System.out.println("Das ist die Aufgabe 7: ");
		System.out.println(a & b & c);
		System.out.println(a || b || c);
		System.out.println(a & b || c);
		System.out.println(a || b & c);
		System.out.println( a ^ b && c);
		System.out.println( a ^ b || c);
	}
}